struct Tarea {
	int ID_Tarea;
	char *Nombre;
	char Estado;
};


void set_ID_Tarea(struct Tarea, int);
void set_Nombre(struct Tarea, char* );
void set_Estado(struct Tarea, char );
int get_ID_Tarea(struct Tarea);
char* get_Nombre(struct Tarea);
char get_Estado(struct Tarea);
void crear_Tarea(struct Tarea, char, char* , int);
void ejecutar_Tarea(struct Tarea);