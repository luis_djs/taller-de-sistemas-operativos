#include "scheduler.h"
#include <stdio.h>

void set_ID_Tarea(struct Tarea t, int ID){
	t.ID_Tarea = ID;

}

void set_Nombre(struct Tarea t, char* nombre){
	t.Nombre=nombre;

}

void set_Estado(struct Tarea t, char estado){
	t.Estado=estado;

}

int get_ID_Tarea(struct Tarea t){
	return t.ID_Tarea;

}

char* get_Nombre(struct Tarea t){
	return t.Nombre;

}

char get_Estado(struct Tarea t){
	return t.Estado;

}

void crear_Tarea(struct Tarea t,char estado,char * nombre,int ID_tarea){
    t.Estado = estado;
    t.Nombre = nombre;
    t.ID_Tarea = ID_tarea;

}

void ejecutar_Tarea(struct Tarea t){
    printf("Ejecutando");
}

 int main(int argc, char *argv[])
{
	printf("Ejecutando inicio\n");
        struct Tarea tarea1;
        printf("1 instancia creada\n");
	    crear_Tarea(tarea1,'A',"Reproducir_Musica",001);
        printf("2 tarea inicializada\n");
        ejecutar_Tarea(tarea1);
        printf("Ejecutando tarea %s ¡FELICIDADES!");
}
